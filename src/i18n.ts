import { createI18n } from 'vue-i18n'
import { deepmerge } from "deepmerge-ts";

import * as enCommon from '../i18n/en-US/common.json'
import * as enSkill from '../i18n/en-US/skill.json'
import * as enItem from '../i18n/en-US/item.json'
import * as jaCommon from '../i18n/ja-JP/common.json'
import * as jaItem from '../i18n/ja-JP/item.json'
import * as jaSkill from '../i18n/ja-JP/skill.json'

const enUS = deepmerge(enCommon, enSkill, enItem)
const jaJP = deepmerge(jaCommon, jaSkill, jaItem)

const i18n = createI18n({
  locale: 'en-US',
  messages: {
    'en-US': enUS,
    'ja-JP': jaJP,
    // 'ko-KR': koKR
  }
})

export default i18n