import { computed, ref } from "vue";
import { machine } from "../module/machine";
import type { FunctionOutput, SkillInput } from "../module/machine/types";

export function helper(jobJson) {
  const machineInput = transformToMachineInput(jobJson);

  const instance = machine(machineInput);

  const data = ref<Map<string, any>>(new Map());

  const totalAssignedPoints = computed<number>(() => {
    return Array.from(data.value.values()).reduce<number>((accum, elem) => {
      accum += elem.curPoint;
      return accum;
    }, 0);
  });

  mergeData(instance.reset());

  function mergeData(pointData: FunctionOutput): any {
    data.value = new Map(
      pointData.map((output) => {
        return [
          output.id,
          {
            ...jobJson.find((elem) => elem.id === output.id),
            ...output,
          },
        ];
      })
    );
  }

  function incrementSkill(id: string) {
    mergeData(instance.increment(id));
  }

  function decrementSkill(id: string) {
    mergeData(instance.decrement(id));
  }

  function resetSkills() {
    mergeData(instance.reset());
  }

  return {
    // instance,
    data,
    totalAssignedPoints,
    incrementSkill,
    decrementSkill,
    resetSkills,
  };
}

function transformToMachineInput(data): SkillInput[] {
  return data.map((skill) => ({
    id: skill.id,
    maxPoint: skill.maxPoint,
    preRequisiteSkills: skill.prereqSkill || [],
    preRequisiteAssignedPoints: skill.prereqPoints || 0,
  }));
}
