export interface MachineInstance {
  increment: (id: string) => FunctionOutput;
  decrement: (id: string) => FunctionOutput;
  reset: () => FunctionOutput;
}

export type FunctionOutput = Array<SkillOutput>;

export interface PrerequisiteSkill {
  id: string;
  minPoint: number;
}

export interface SkillInput {
  id: string;
  maxPoint: number;
  preRequisiteSkills: Array<PrerequisiteSkill>;
  preRequisiteAssignedPoints: number;
}

export interface SkillOutput {
  id: string;
  curPoint: number;
  maxPoint: number;
  canIncrement: boolean;
  canDecrement: boolean;
}




