import type {
  FunctionOutput,
  MachineInstance,
  PrerequisiteSkill,
  SkillInput,
  SkillOutput
} from "./types"

interface Skills extends SkillInput, SkillOutput {}

export function machine (input: SkillInput[]): MachineInstance {
  const skills = new Map<string, Skills>(input.map(skill => ([
    skill.id, {
      ...skill,
      curPoint: 0,
      canIncrement: false,
      canDecrement: false
    }]))
  )

  let points: Array<string> = []

  function resetSkillOutput () {
    points = []
    skills.forEach(skill => {
      skill.curPoint = 0
      skill.canIncrement = true
      skill.canDecrement = true
    })
  }

  function recalculateAllSkill () {
    let totalAssignedPoints = points.length;

    const skillsArray = Array.from(skills.values())

    // allocate each skill's points
    skillsArray.forEach(skill => {
      skill.curPoint = points.filter(id => id === skill.id).length
    })

    // build a sorted list of skills with preRequisiteAssignedPoints
    const preReqAssignedPoints = skillsArray
      .filter(skill => skill.preRequisiteAssignedPoints > 0 && skill.curPoint > 0)
      .sort((a, b) => a.preRequisiteAssignedPoints - b.preRequisiteAssignedPoints)
      
    skillsArray.forEach(skill => {
      const { preRequisiteAssignedPoints, preRequisiteSkills } = skill
      
      skill.canIncrement = (() => {
        if (preRequisiteAssignedPoints > 0 && totalAssignedPoints < preRequisiteAssignedPoints) {
          return false
        }

        if (preRequisiteSkills.length) {
          if (preRequisiteSkills.some(recursiveCheckCanIncrement)) {
            return false
          }
        }

        return true
      })()

      skill.canDecrement = (() => {
        // special case where there's no points assigned. It returns true because the .canDecrement flag
        // is used for letting the user know that there's points allocated and it cannot be decrement due
        // to other skills having pre-requisite skill points and some points have already been assigned.
        if (skill.curPoint === 0) {
          return true
        }

        // if there are no skills that require pre-requisite assigned points, then all skills can be decrement
        if (preReqAssignedPoints.length > 0) {
          const highestPreReqSkill = preReqAssignedPoints[preReqAssignedPoints.length - 1]

          const highestIndexForCurrentSkillPoint = points.lastIndexOf(skill.id) + 1
          if (highestIndexForCurrentSkillPoint <= highestPreReqSkill.preRequisiteAssignedPoints) {
            return false
          }
        }

        // find skills that have this skill as the pre-requisite and have points assigned
        const skillsWithThisSkillAsPreRequisite = skillsArray.filter(elem => 
          elem.preRequisiteSkills.find(preReq => preReq.id === skill.id) !== undefined
        )
        if (skillsWithThisSkillAsPreRequisite.some(elem => elem.curPoint > 0)) {
          return false
        }

        return true
      })()
    })
  }

  function recursiveCheckCanIncrement (preReqSkill: PrerequisiteSkill): boolean {
    const skill = skills.get(preReqSkill.id)
    const curPreReqSkillPoints = skill.curPoint

    if (preReqSkill.minPoint > curPreReqSkillPoints) {
      return true
    }

    if (skill.preRequisiteSkills.length) {
      if (skill.preRequisiteSkills.some(recursiveCheckCanIncrement)) {
        return true
      }
    }

    return false
  }

  function getFunctionOutput (): FunctionOutput {
    recalculateAllSkill()
    return Array.from(skills.values()).map(({ id, curPoint, maxPoint, canIncrement, canDecrement }) => ({
      id,
      curPoint,
      maxPoint,
      canIncrement,
      canDecrement
    })) 
  }

  function increment (id: string): FunctionOutput {
    const skill = skills.get(id)
    if (skill.canIncrement && skill.curPoint < skill.maxPoint) {
      points.push(skill.id)
    }
    return getFunctionOutput()
  }

  function decrement (id: string): FunctionOutput {
    const skill = skills.get(id)
    if (skill.canDecrement && skill.curPoint > 0) {
      const lastIndexOfId = points.lastIndexOf(id)
      points.splice(lastIndexOfId, 1)
    }
    return getFunctionOutput()
  }

  function reset(): FunctionOutput {
    resetSkillOutput()
    return getFunctionOutput()
  }

  return {
    increment,
    decrement,
    reset
  }
}