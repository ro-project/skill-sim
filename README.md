# README

### How do I get set up?

1. run `npm install`
2. run `npm run start`
3. open browser and point it to `http://localhost:1234`

### Contribution guidelines

- All text meant for display should be inside /i18n/ respective folder

### TODO

- [ ] find out how to draw lines connection to skills
  - https://medium.com/@krutie/building-a-dynamic-tree-diagram-with-svg-and-vue-js-a5df28e300cd
- [ ] get description of skills and details and show in popover
- [ ] make sure this works on mobile and tablet too
- [ ] validator for machine's input
- [ ] unit test the machine output
- [ ] install bootstrap 5 css via CDN or whatever you're used to working with
- [ ] block iframes
- [ ] url parsing and manipulation
  - parse class skill build, e.g. http://localhost:1234/en-US/archer/{some gibberish hex voodoo}
- [ ] allow embedding of allocated skill tree. e.g.
  - https://localhost:1234/en-us/acolyte/embed/
  - https://localhost:1234/embed/en-us/acolyte/embed
- [ ] allow playback of skill addition for making guides or animated gif (melz's idea)
- [ ] offer preset profiles? have to login lol, need db? charge $$$$$ MUYAHHAHA
